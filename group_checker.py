#!/usr/bin/env python3

import argparse
import datetime
import ldap_parser
import nice_crawler
import sys

from collections import defaultdict


URL_OVERVIEW = 'https://www.mpi-inf.mpg.de/departments/algorithms-complexity/people/'
URL_GROUP_OVERVIEW = 'https://www.mpi-inf.mpg.de/departments/algorithms-complexity/research/'
EXPECT_GROUPS = {
    'Algorithmic Game Theory',
    'Combinatorics, Computing, and Randomness',
    'Combinatorial Optimization',
    'Computational Geometry',
    'Theory of Distributed and Embedded Systems',
    'Parameterized Algorithms and Complexity',
}

HOMEPAGE_PREFIX = 'https://people.mpi-inf.mpg.de/~'

WHITELISTED_PEOPLE = {
    'Attila Kinali', 'Attila Dogan',  # Changing names; system has a hard time
    'Nick Fischer',  # Weird department affiliation
}


def determine_relevant_people():
    people = ldap_parser.compute_current_people()
    relevant_people, _ = ldap_parser.get_relevant_people(people)
    # skipped_people already get reported in homepage_checker.
    return [person['cn'][0] for person in relevant_people]


def determine_overview_people():
    success, result = nice_crawler.get(URL_OVERVIEW)
    assert success, (URL_OVERVIEW, result)
    soup = nice_crawler.make_soup(result)
    people = []
    categories = soup.select('.ym-cbox > .frame.box')
    assert len(categories) >= 3
    headers = []
    for category in categories:
        cat_name = category.find('h1').text.strip()
        headers.append(cat_name)
        if 'irector' in cat_name or 'Secretar' in cat_name:
            continue
        for li_elem in category.find_all('li', {'class': 'name'}):
            name_raw = li_elem.text.replace('\xa0', ' ').strip()
            parts = name_raw.split(', ')
            assert len(parts) == 2, (URL_OVERVIEW, li_elem)
            people.append('{} {}'.format(parts[1], parts[0]))
    assert bool(people), (URL_OVERVIEW, headers)
    return people


def determine_group_urls():
    success, result = nice_crawler.get(URL_GROUP_OVERVIEW)
    assert success, (URL_GROUP_OVERVIEW, result)
    soup = nice_crawler.make_soup(result)
    group_names = set()
    group_urls = set()
    for row in soup.find_all('dl', {'class': 'special_format'}):
        link_elems = row.find_all('a')
        assert len(link_elems) == 1, (URL_GROUP_OVERVIEW, row)
        group_names.add(link_elems[0].text.strip())
        group_urls.add(nice_crawler.resolve(URL_GROUP_OVERVIEW, link_elems[0]['href']))
    assert len(group_names) == len(group_urls), ('#Names != #URLs?!', URL_GROUP_OVERVIEW, group_names, group_urls)
    assert group_names == EXPECT_GROUPS, (URL_GROUP_OVERVIEW, group_names, group_urls)
    return group_urls


def collect_people_from(group_urls, whitelist_used):
    # People can and will be in multiple groups; this is not an error.
    people = set()
    for page in group_urls:
        # page can be e.g. 'https://www.mpi-inf.mpg.de/departments/algorithms-complexity/research/combinatorics-computing-and-randomness/'
        success, result = nice_crawler.get(page)
        assert success, (page, result)
        soup = nice_crawler.make_soup(result)
        tables = 0
        for table in soup.find_all('table', {'class': 'contenttable'}):
            tables += 1
            for person in table.find_all('li'):
                person_name = person.text.replace('\xa0', ' ').strip()
                people.add(person_name)
                person_a = person.find('a')
                person_href = None if person_a is None else person_a.get('href')
                url_is_normal = person_href is not None and person_href.startswith(HOMEPAGE_PREFIX) and person_href[len(HOMEPAGE_PREFIX):].rstrip('/').isalpha()
                if not url_is_normal:
                    if person_name not in WHITELISTED_PEOPLE:
                        print('Link on {} for {} is weird: {}'.format(
                            page, person_name, person_href))
                    else:
                        whitelist_used.add(person_name)
        assert tables >= 2, (page, tables, 'Too few tables?!')  # The tables have turned! ;-)

    return people


def run_with_options(_options):
    whitelist_used = set()
    ldap_people = determine_relevant_people()
    overview_people = determine_overview_people()
    group_urls = determine_group_urls()
    group_people = collect_people_from(group_urls, whitelist_used)

    seen = defaultdict(set)
    for person in ldap_people:
        seen[person].add('ldap')
    for person in overview_people:
        seen[person].add('overview')
    for person in group_people:
        seen[person].add('group')
    systems_expected = {'ldap', 'overview', 'group'}

    for name, systems_actual in sorted(seen.items()):
        if systems_actual == systems_expected:
            # All good.
            continue
        if name in WHITELISTED_PEOPLE:
            # Ignore.
            whitelist_used.add(name)
            continue
        print('Saw "{}" only in system(s) {}; but not in {}.'.format(
            name,
            ', '.join(systems_actual),
            ', '.join(systems_expected - systems_actual),
        ))
    for name in sorted(WHITELISTED_PEOPLE.difference(whitelist_used)):
        print('Whitelist-entry for "{}" no longer needed?'.format(name))


def parse_argv(argv):
    parser = argparse.ArgumentParser(prog=argv[0], description='Group Checker')
    # parser.add_argument('--uid', action='append', help='Consider only the user(s) of the given uid(s).  (Default: consider all D1 researchers and directors)')
    # No meaningful options currently
    options = parser.parse_args(argv[1:])
    return options


def run_argv(argv):
    print('BEGIN: {}'.format(datetime.datetime.now()))
    old_wait = nice_crawler.NICE_WAIT_TIME
    nice_crawler.NICE_WAIT_TIME = min(3, nice_crawler.NICE_WAIT_TIME)
    try:
        run_with_options(parse_argv(argv))
    finally:
        print('END: {}'.format(datetime.datetime.now()))
        nice_crawler.NICE_WAIT_TIME = old_wait


if __name__ == '__main__':
    run_argv(sys.argv)
