#!/usr/bin/env python3

import homepage_checker

urls = homepage_checker.read_bad_urls()
print('Size before:', len(urls))
urls = {k: v for k, v in urls.items() if len(v) > 1}
print('Size after:', len(urls))
homepage_checker.write_bad_urls(urls)
