#!/usr/bin/env python3

import homepage_checker

urls = homepage_checker.read_bad_urls()
print('Read {} urls:\n{}'.format(len(urls), urls.keys()))
