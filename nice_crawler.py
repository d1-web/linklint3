#!/usr/bin/env python3

import bs4
import random
import requests
import time
import urllib.parse


# Amount of seconds to wait between each download.
NICE_WAIT_TIME = 21
# Minimal sleep time for grace, in seconds.
MIN_SLEEP_TIME = 0.1

DEBUG = False
HEADERS = {'User-Agent': 'mpi-d1-linklinter/3.0 contact/d1-web-adm-mails@mpi-inf.mpg.de'}
GET_KWARGS = {'headers': HEADERS, 'cookies': {'accept': 'true'}, 'timeout': 40}

_last_nice_poll = 0


def get(url):
    '''
    Access the given URL, and either return the content as a bytestring, or `None`
    if any error occurs.  This function delays invocations in order to guarantee a certain rate limiting.
    '''
    global _last_nice_poll
    if DEBUG:
        print("[NICE] requesting ticket for {} …".format(url))
    now = time.time()
    until = _last_nice_poll + NICE_WAIT_TIME
    while now < until:
        to_sleep = max(MIN_SLEEP_TIME, until - now)
        if DEBUG:
            print("[NICE] sleep for {} seconds.".format(to_sleep))
        time.sleep(to_sleep)
        now = time.time()
    _last_nice_poll = time.time()
    if DEBUG:
        print("[NICE] got ticket!  Fetching data …")
    try:
        r = requests.get(url, **GET_KWARGS)
        r.raise_for_status()
    except requests.exceptions.RequestException as e:
        # This includes ConnectionError, HTTPError, Timeout, TooManyRedirects.
        if DEBUG:
            print("[NICE] Failed: {}".format(e))
        return (False, str(e))
    return (True, r.content)


def make_soup(content):
    try:
        return bs4.BeautifulSoup(content, 'lxml')
    except:
        # Ehhh, whatever
        return None


def resolve(document_url, href):
    return urllib.parse.urljoin(document_url, href)


def canonicalize_url(url):
    url = urllib.parse.splittag(url)[0]  # Strip the '#foobar' part
    if url.endswith('index.html'):
        url = url[:-len('index.html')]
    url.rstrip('/')
    return url


def check_urls(url_set, skip_content=False):
    url_list = list(url_set)
    random.shuffle(url_list)
    instantiations = dict()
    results = dict()
    for url in url_list:
        canonical_url = canonicalize_url(url)
        previous_url = instantiations.get(canonical_url)
        if previous_url is not None:
            results[url] = results[previous_url]  # reuse
            # This canonicalization reduces the number of downloaded webpages from roughly 1080 to roughly 980.
            continue
        instantiations[canonical_url] = url
        this_result = get(url)
        if skip_content and this_result[0]:
            # URL seems to be valid, and the caller requested to save memory.
            # Therefore, discard the content.
            this_result = (True, '')
        results[url] = this_result
    return results


def printable_url(url):
    return urllib.parse.quote_plus(url, safe=':/#~')


def check_verbosely(urls):
    for url in urls:
        print('Checking {} ...'.format(url))
        is_good, result = get(url)
        if is_good:
            print('\tGOOD')
        else:
            print('\tBAD: ' + result)


if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        check_verbosely(sys.argv[1:])
    else:
        check_verbosely(['https://incomplete-chain.badssl.com/', 'https://tls-v1-2.badssl.com:1012/', 'https://wrong.host.badssl.com/'])
