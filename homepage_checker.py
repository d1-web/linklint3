#!/usr/bin/env python3

# Usage: Put a line into your crontab, saying:
# path/to/homepage_checker.py --update-url-cache --send-mail

import argparse
import datetime
import hashlib
import json
import ldap_parser
import nice_crawler
import os
import re
import smtplib
import sys
import time

from collections import defaultdict
from email.mime.text import MIMEText


WHITELISTED_PEOPLE = {
    'aantonia',  # basically a redirect
    'askarlat',  # student, I guess?
    'pmisra',  # basically a redirect
    'skisfalu',  # basically a redirect
    'tgouleak',  # a delayed redirect
    'vnakos',  # redirect
    'mmedina', # 'dummy' account for SVN usage
}
## Because they arrive far in the future, whitelist them until then:
#if datetime.datetime.now() < datetime.datetime(2020, 8, 2):
#    WHITELISTED_PEOPLE.add('someuid')

WEBSITE_PATH_DIRECTORY = '/www/inf-homepage/'
WEBSITE_URL_FORMAT = 'https://people.mpi-inf.mpg.de/~{}/'
WEBSITE_OLD_THRESHOLD = 31536000  # seconds = 1 year.
BANNED_HREF_PREFIXES = ['#', 'mailto:', 'javascript:']
BAD_URL_CACHE = os.path.abspath(os.path.dirname(__file__)) + '/bad_urls.json'
REPORT_BAD_URL_THRESHOLD = 3
DUMP_EXTERNAL_URLS = os.path.abspath(os.path.dirname(__file__)) + '/external-urls.json'
LABEL_SANITIZE_RE = re.compile('^- ?|[\r\n\t\xa0]|(?<= ) +|^ | $', re.MULTILINE)

SKIP_PREFIXES = [
    # Server doesn't like us, or robots.txt forbids it anyway.
    "https://dl.acm.org/",
    "http://dl.acm.org/",
    "https://portal.acm.org/",
    "http://portal.acm.org/",
    "https://dx.doi.org/",
    "http://dx.doi.org/",
    "https://doi.acm.org/",
    "http://doi.acm.org/",
    "https://www.sciencedirect.com/science/article/pii/S",
    "http://www.sciencedirect.com/science/article/pii/S",
    # Incomplete certificate chain
    "http://eatcs.org",
    "https://eatcs.org",
    # GoDaddy-servers always respond with a TCP RST *after* the first few bytes for the first connection.
    # How the hell can it be so broken?!
    "http://www.antoniosantoniadis.net",
    "https://www.antoniosantoniadis.net",
    "http://christos.me",
    "https://christos.me",
    # Does not support TLS 1.2+
    "http://www.mmci.uni-saarland.de/",
    "https://www.mmci.uni-saarland.de/",
    # Too stupid to maintain TLS certs reliably
    "http://www.info.uaic.ro",
    "https://www.info.uaic.ro",
]
SKIP_URLS = [
    # Can't handle password-protected file.
    "https://tods.mpi-inf.mpg.de/restricted/pals-presentation.mp4",
    # URLs changed but templates didn't (not our fault)
    "https://www.mpi-inf.mpg.de/services/information-services-technology/",
    "https://www.mpi-inf.mpg.de/services/building-and-technical-support/",
    "https://www.mpi-inf.mpg.de/services/public-relations/"
]
SKIP_URL_RE = re.compile('|'.join(
    ['^{}'.format(re.escape(p)) for p in SKIP_PREFIXES] +
    ['^{}'.format(re.escape(u)) for u in SKIP_URLS] +
    []
))


COMPLAINT_SMTP_SERVER = 'localhost'
COMPLAINT_FROM = 'd1-web-adm-mails@mpi-inf.mpg.de'
COMPLAINT_SUBJECT = '[linklint3] Issues found on your homepage'
COMPLAINT_BCC = ['coupette@mpi-inf.mpg.de']
COMPLAINT_BODY = '''\
Hi {givenName[0]},

I think there are some issues with your homepage.  Could you look into them?

{spaced_complaints}
Feel free to reply if any of these issues do not actually apply.

Cheers,
Link linter on pandora, run by the d1 web-admin\
'''

COMPLAINT_NO_WEBSITE = '''\
Your homepage directory seems to be missing.  I expected it in this path:
    {path}
However, it does not exist.  If you want a homepage, you should probably
ask the servicedesk about this.  Or tell me to stop reminding you about it.
'''

COMPLAINT_OLD_WEBSITE = '''\
I've noticed that your website is at least one year old.  {age_days} days, to be exact.
You can silence this program for a year by modifying your website.
If you don't think it needs modification, you may save an unchanged copy.
If you do so, however, please keep in mind that your professional homepage is
part of the institute's public appearence.  Here is how you can access it:
    https://plex.mpi-klsb.mpg.de/pages/viewpage.action?pageId=13908009#UserHomepages-Accessingyourhomepageonthefilesystem
In case this was a false alarm, here's where I looked:
    {path}
Or, if you prefer a redirect, try replacing the HTML-file with a snippet like this:
	<meta http-equiv="refresh" content="0; url=http://www.zamonien.de/autor.php" />
	<p>If you do not get redirected automatically, please click <a href="http://www.zamonien.de/autor.php">here</a>.</p>
And tell me that I should deactivate freshness checks for your homepage.
'''

COMPLAINT_INACCESSIBLE_WEBSITE = '''\
I'm having trouble accessing your homepage.  Either the page doesn't load or is
unparsable. Maybe I'm looking in the wrong place?
This is where I expected the homepage to be:
    {url}
This is the detailed technical error string:
    {error}
'''

COMPLAINT_BROKEN_WEBSITE = '''\
I'm having trouble accessing your homepage.  I can't find the string
"Homepage" anywhere on it.  Maybe I'm looking in the wrong place?
This is where I expected the homepage to be:
    {url}
'''

# This only applies to old users that *still* haven't updated their old pages.
COMPLAINT_BLANK_WEBSITE = '''\
It seems like this is the template, without any of your information filled in.
Specifically, I could find neither "{givenName[0]}" nor "{sn[0]}" on it!
Maybe I'm looking in the wrong place?  This is where I expected the homepage to be:
    {url}
Please keep in mind that your professional homepage is part of the institute's
public appearence.  Thus, the d1-web-admin(s) reserve the right to ask you to
change or improve parts of your website in order to maintain the institute's
professional web presence.  Here is how you can access it:
    https://plex.mpi-klsb.mpg.de/pages/viewpage.action?pageId=13908009#UserHomepages-Accessingyourhomepageonthefilesystem
In case this was a false alarm, here's where I looked:
    {path}
'''

TEMPLATISH_STRINGS = [
    '#departmentnumber#',
    '#departmentname#',
    '#researcharea#',
    '#room#',
    'Item 1 of your list of research interests',
    'Item 2 of your list of research interests',
    'Item 3 of your list of research interests',
    'Lastname, F.: The Title and the rest of the nice bibliography data',
    'January 200N',
    'Month 199N',
    'Month 200N',
    'Gymnasium, X Y',
    'This could be a list of links',
    '#officephone#',
    '#officefax#',
]

# This will be sent pretty much immediately to new users.
# So try to be welcoming.
COMPLAINT_TEMPLATE_WEBSITE = '''\
It seems like this is the template, with some of your information missing.
Specifically, the string "{templatish}" looks a lot like the template.
Please take some time in the coming days to fill-in your data.  If you use
an MPI-issued laptop, you can probably directly access the mpi-inf NFS [1].
If not, you're forced to setup [2, 3] SSH, which is probably a good idea anyway.
After that, please tell your group coordinator(s) (and maybe me, too)
to add you on the respective group pages, e.g. [4].
If you think this is a false alarm, please look at this URL first:
    {url}

[1] https://plex.mpi-klsb.mpg.de/display/Documentation/User+Homepages#UserHomepages-Accessingyourhomepageonthefilesystem
[2] https://plex.mpi-klsb.mpg.de/display/Documentation/Generating+SSH-Key
[3] https://plex.mpi-klsb.mpg.de/display/Documentation/Access+From+Outside
[4] https://www.mpi-inf.mpg.de/departments/algorithms-complexity/research/combinatorics-computing-and-randomness/
'''

COMPLAINT_TEMPLATE_IMAGE = '''\
Your website seems to use for your photograph the literal file "firstnamelastname.jpg", and that file seems to be
the original placeholder (hash {hash}).
Please either:
- Use your actual photo, that would be great!
- Or decide that your homepage shall not have a photo; that is fine, too.
'''

COMPLAINT_BAD_LINK = '''\
There is a dead link labeled “{label}”.  The URL is:
    {url}
This is the exact error I encountered when looking at the URL:
    {error}
'''

EMAIL_EXCEPTIONS = {
    'pkolev',  # JS string arithmetic
    'wellnitz',  # JS string arithmetic
    'apandey',  # too general link, but whatever
    'cosmina',  # too general link, but whatever
    'karrenba',  # CAPTCHA by reading comprehension (I like it!)
    'mehlhorn',  # indirection
    'samiri',  # basically retired anyway
}
EMAIL_TEMPLATE = 'href="paste the URL'

COMPLAINT_EMAIL_TEMPLATE = '''\
Your e-mail address is inaccessible from outside networks.
Please take a look at the following file:
    {path}
Read it, and follow the instructions starting with:
    How to find the unique URL for your email address …
The result should be something like this:
    <a href="https://domino.mpi-inf.mpg.de/intranet/mpii/people.nsf/email!ReadForm&UID=99999&Name=Mustermann%2C%20Max">Get my email address via email</a><br />
Thank you.
'''

EMAIL_EVIL_UID = 'UID=99999'

COMPLAINT_EMAIL_COPIED_UID = '''\
Your e-mail address is *still* inaccessible from outside networks.
It's still about this file:
    {path}
Please don't copy my example verbatim; actually follow the instructions you find there.
'''

EMAIL_EVIL_NAME = 'Name=Mustermann'

EMAIL_GOOD_PREFIX = 'https://domino.mpi-inf.mpg.de/intranet/mpii/people.nsf/email!ReadForm&'

COMPLAINT_EMAIL_COPIED_NAME = COMPLAINT_EMAIL_COPIED_UID


def determine_relevant_people(options):
    if options.uid:
        return ldap_parser.fill_people(options.uid)

    people = ldap_parser.compute_current_people()
    relevant_people, skipped_people = ldap_parser.get_relevant_people(people)
    if bool(skipped_people):
        skipped_cns = list(map(lambda person: person['cn'][0], skipped_people))
        print('Skipped due to role: {}\n'.format(skipped_cns))

    filtered_any = False
    filtered_relevant_people = []
    for person in relevant_people:
        if person['uid'][0] in WHITELISTED_PEOPLE:
            print('Skipped due to whitelist: {} ({})'.format(person['uid'][0], person['cn'][0]))
        else:
            filtered_relevant_people.append(person)

    return filtered_relevant_people


def check_homepage_file(person):
    '''
    Given a person, check whether the homepage file exists, and return whether
    it makes sense to look at the homepage through a server (i.e., it exists).
    Also, complain about anything that seems off (i.e., too old, or
    doesn't exist).
    '''
    assert os.path.isdir(WEBSITE_PATH_DIRECTORY)
    homepage_file = WEBSITE_PATH_DIRECTORY + person['uid'][0] + '/index.html'
    person['homepage_file'] = homepage_file
    if not os.path.exists(homepage_file):
        # File does not exist, but should.
        person['complaints'].append(COMPLAINT_NO_WEBSITE.format(path=homepage_file, **person))
        return False

    current_time = time.time()
    last_modified = os.path.getmtime(homepage_file)
    if (current_time - last_modified) > WEBSITE_OLD_THRESHOLD:
        # Homepage is too old.
        age_days = int((current_time - last_modified) / (24 * 3600) + 0.5)
        person['complaints'].append(COMPLAINT_OLD_WEBSITE.format(path=homepage_file, age_days=age_days, **person))
        pass

    return True


def check_mail_link(person):
    '''
    Given a person, check whether the linked email is correct.
    Specifically, see whether this string appears:
        href="paste the URL
    because it's part of the template, and only visible from the outside.
    This sucks.
    '''
    if person['uid'][0] in EMAIL_EXCEPTIONS:
        return
    assert os.path.isdir(WEBSITE_PATH_DIRECTORY)
    filename = None
    for basename in ['personal.inc', 'email.inc', 'index.html']:
        filename = WEBSITE_PATH_DIRECTORY + person['uid'][0] + '/' + basename
        if os.path.isfile(filename):
            break
    else:
        print('Could not find email address page for {}, last attempt: {}'.format(person['uid'][0], filename))
        return

    with open(filename, 'r') as fp:
        content = fp.read()
    if EMAIL_TEMPLATE in content:
        person['complaints'].append(COMPLAINT_EMAIL_TEMPLATE.format(path=filename, **person))
    elif EMAIL_EVIL_UID in content:
        person['complaints'].append(COMPLAINT_EMAIL_COPIED_UID.format(path=filename, **person))
    elif EMAIL_EVIL_NAME in content:
        person['complaints'].append(COMPLAINT_EMAIL_COPIED_NAME.format(path=filename, **person))
    elif EMAIL_GOOD_PREFIX in content:
        pass  # Great!
    elif person['mail'][0] in content:
        pass  # Ehh, whatever, it's their choice.
    elif (person['istEmailName'][0] + '@mpi-inf.mpg.de') in content:
        pass  # Ehh, whatever, it's their choice.
    else:
        print('No e-mail address in: ' + filename)


def crawl_homepage_url(person):
    '''
    Given a person, download their homepage, and extract all links.
    Complain about any irregularities.
    '''
    website_url = WEBSITE_URL_FORMAT.format(person['uid'][0])
    person['homepage_url'] = website_url
    success, website_raw = nice_crawler.get(website_url)
    if success:
        website_soup = nice_crawler.make_soup(website_raw)
        if website_soup is not None:
            website_text = website_soup.body.text
        else:
            success = False
            website_raw = 'HTML is broken and unparsable (?)'
    if not success:
        person['complaints'].append(COMPLAINT_INACCESSIBLE_WEBSITE.format(url=website_url, error=website_raw, **person))
        return
    if b'Homepage' not in website_raw:
        person['complaints'].append(COMPLAINT_BROKEN_WEBSITE.format(url=website_url, **person))
        return

    assert bool(website_soup)
    assert bool(website_text)

    if person['givenName'][0] not in website_text and person['sn'][0] not in website_text:
        person['complaints'].append(COMPLAINT_BLANK_WEBSITE.format(url=website_url, path=person['homepage_file'], **person))
        return  # No need to check the individual links then
    for x in TEMPLATISH_STRINGS:
        if x in website_text:
            person['complaints'].append(COMPLAINT_TEMPLATE_WEBSITE.format(url=website_url, templatish=x, **person))
            return  # No need to check the individual links then
    if website_soup.find('img', src='firstnamelastname.jpg') is not None:
        image_file = WEBSITE_PATH_DIRECTORY + person['uid'][0] + '/firstnamelastname.jpg'
        with open(image_file, "rb") as fp:
            readable_hash = hashlib.sha256(fp.read()).hexdigest().lower();
        if readable_hash == 'e3a5113d0e2f29368de70d3c90177c9560100e90fc390f4385608a4674d0bfb2':
            person['complaints'].append(COMPLAINT_TEMPLATE_IMAGE.format(hash=readable_hash, **person))

    for tag in website_soup.find_all('a'):
        if not tag.has_attr('href'):
            continue
        if any(tag['href'].startswith(s) for s in BANNED_HREF_PREFIXES):
            continue
        person['links'].append((nice_crawler.resolve(website_url, tag['href']), tag.text[:90]))
    for tag in website_soup.find_all('img'):
        if not tag.has_attr('src'):
            continue
        person['links'].append((nice_crawler.resolve(website_url, tag['src']), tag.text[:90]))


def check_urls(url_set):
    results = dict()
    query_set = set()
    for url in url_set:
        if bool(SKIP_URL_RE.match(url)):
            results[url] = (True, '')
            continue  # skip
        query_set.add(url)
    results.update(nice_crawler.check_urls(query_set, skip_content=True))
    return results


def read_bad_urls():
    if not os.path.exists(BAD_URL_CACHE):
        return dict()
    with open(BAD_URL_CACHE, 'r') as fp:
        bad_urls = json.load(fp)

    def wrap(v):
        if isinstance(v, str):
            return [v]
        elif isinstance(v, list):
            return v
        else:
            raise AssertionError('Expected str or list?!', k, type(v))
    return {k: wrap(v) for k, v in bad_urls.items()}


def write_bad_urls(bad_urls):
    with open(BAD_URL_CACHE, 'w') as fp:
        # indent=0 inserts newlines, but no tabs/spaces.
        json.dump(bad_urls, fp, sort_keys=True, indent=0)


class PreparedMail:
    def __init__(self, to, body):
        assert isinstance(to, str), to
        assert isinstance(body, str), body
        self.to = to
        self.body = body
        self.subject = COMPLAINT_SUBJECT

    def send(self):
        msg = MIMEText(self.body)
        msg['Subject'] = self.subject
        msg['From'] = COMPLAINT_FROM
        msg['To'] = self.to
        s = smtplib.SMTP('localhost')
        s.sendmail(COMPLAINT_FROM, [self.to] + COMPLAINT_BCC, msg.as_string())
        # s.sendmail(COMPLAINT_FROM, COMPLAINT_BCC, msg.as_string())


def prepare_mail(person):
    spaced_complaints = '\n'.join(person['complaints'])
    mail_body = COMPLAINT_BODY.format(spaced_complaints=spaced_complaints, **person)
    return PreparedMail(person['mail'][0], mail_body)


def run_with_options(options):
    people = determine_relevant_people(options)

    # Read their homepages:
    for person in people:
        del person['istEmployeeAffiliation']
        person['complaints'] = list()
        person['links'] = list()
        if check_homepage_file(person):
            crawl_homepage_url(person)
            check_mail_link(person)

    # Check all links:
    if options.crawl_external:
        url_set = set()
        for person in people:
            url_set.update(map(lambda x: x[0], person['links']))
        bad_urls_prev = read_bad_urls()
        url_validity_now = check_urls(url_set)
        newly_broken_urls = dict()
        bad_urls_now = dict()  # {url: [reason] for (url, (success, reason)) in url_validity_now.items() if not success}
        for (url, (success, new_reason)) in url_validity_now.items():
            if success:
                # All is fine
                continue
            prev_reasons = bad_urls_prev.get(url)
            if prev_reasons is None:
                newly_broken_urls[url] = new_reason
                bad_urls_now[url] = [new_reason]
            else:
                bad_urls_now[url] = [new_reason] + prev_reasons
        del url_validity_now
        report_bad_urls = dict()
        for (url, reasons) in bad_urls_now.items():
            if len(reasons) >= REPORT_BAD_URL_THRESHOLD:
                report_bad_urls[url] = reasons[0]

        if options.update_url_cache:
            write_bad_urls(bad_urls_now)
        del bad_urls_now
        del bad_urls_prev
        for person in people:
            for url, label in person['links']:
                error = report_bad_urls.get(url)
                if error is None:
                    continue
                url = nice_crawler.printable_url(url)
                label = LABEL_SANITIZE_RE.sub('', label)
                person['complaints'].append(COMPLAINT_BAD_LINK.format(url=url, label=label, error=error, **person))
        del report_bad_urls
    else:
        # Save the state somehow
        urls = set()
        for person in people:
            urls.update(map(lambda x: x[0], person['links']))
        urls = list(urls)
        urls.sort()
        with open(DUMP_EXTERNAL_URLS, 'w') as fp:
            json.dump(dict(urls=urls, people=people), fp, sort_keys=True, indent=1)
        # Pretend that all the checks have run
        newly_broken_urls = dict()

    if newly_broken_urls:
        print('newly broken:')
        for url, reason in newly_broken_urls.items():
            print('* {}: {}'.format(url, reason))
    for person in people:
        if person['complaints']:
            the_mail = prepare_mail(person)
            if options.send_mail:
                the_mail.send()
            else:
                print('--- Would send: ---\nTo: {}\nSubject: {}\nBody:\n{}\n---------\n'.format(the_mail.to, the_mail.subject, the_mail.body))


def parse_argv(argv):
    parser = argparse.ArgumentParser(prog=argv[0], description='User Page Checker')
    parser.add_argument('--uid', action='append', help='Consider only the user(s) of the given uid(s).  (Default: consider all D1 researchers and directors)')
    parser.add_argument('--send-mail', action='store_true', help='Send reminder e-mails to people who have old or missing webpages.  (Default: output a note to the terminal)')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--update-url-cache', action='store_true', help='Rewrite local cache of crawl results.  (Default: do not write anything to the harddrive)')
    group.add_argument('--no-crawl-external', dest='crawl_external', action='store_false', help='Do not contact web servers other than people.mpi-inf.mpg.de, and write the urlset to `external-urls.json`.  (Default: validate all links)')
    options = parser.parse_args(argv[1:])
    assert (not options.update_url_cache) or options.crawl_external
    return options


def run_argv(argv):
    print('BEGIN: {}'.format(datetime.datetime.now()))
    try:
        run_with_options(parse_argv(argv))
    finally:
        print('END: {}'.format(datetime.datetime.now()))


if __name__=='__main__':
    run_argv(sys.argv)
