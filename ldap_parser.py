#!/usr/bin/env python3

from collections import defaultdict
import subprocess


DEPARTMENT = 'D1'
LDAP_KEYS = ['cn', 'givenName', 'sn', 'uid', 'istEmployeeAffiliation', 'mail', 'istEmailName']

# Checked from top to bottom
ROLE_RELEVANCE_MAP = [
    ('Guest', False),
    ('Director', True),
    ('Secretary', False),
    ('Research Leader', True),
    ('Junior Research Leader', True),
    ('Senior Researcher', True),
    ('Researcher', True),
    ('Post Doc', True),
    ('PhD Student', True),
    ('Staff Member', True),
    ('Student', False),
    ('HiWi', False),
    ('Masters', False),
    ('Bachelor', False),
    ('Internship', False),
]
ROLE_RELEVANCE_DEFAULT = False


def people_from_query(ldap_query):
    ldap_argv = ['ldaps', '-l', ldap_query] + LDAP_KEYS
    completed_process = subprocess.run(ldap_argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
    split_output = completed_process.stdout.decode('latin-1').split('\n')
    assert split_output[-1] == '', split_output
    people = []
    for person_str in split_output[:-1]:
        split_person = person_str.split(':')
        assert split_person[-1] == '', split_person
        person = defaultdict(list)
        for kv_pair in split_person[:-1]:
            k, v = kv_pair.split('=')
            person[k].append(v)
        people.append(dict(person))
    return people


def compute_current_people():
    ldap_query = '&(istEmployeeAffiliation=*{}*)(!(istIsPseudoAccount=TRUE))(!(istIsDisabled=TRUE))(!(istIsFormer=TRUE))'.format(DEPARTMENT)
    return people_from_query(ldap_query)


def fill_people(uids):
    ldap_query = '|' + ''.join('(uid={})'.format(uid) for uid in uids)
    people = people_from_query(ldap_query)
    missing_uids = set(uids)
    for person in people:
        missing_uids.discard(person['uid'][0])
    if missing_uids:
        raise ValueError('These UIDs seem to be invalid:', missing_uids)
    return people


def get_relevant_affiliations(person):
    # Since people might have multiple affiliations, this function can be used
    # to get one of the highest priority.  If the 'PRIMARY' affiliation is in D1
    # that one is returned. Otherwise all 'NORMAL' aff. in D1 are returned.
    affiliations = list(filter(lambda a: 'D1' in a, person['istEmployeeAffiliation']))

    #Case 1. only one affiliation, it must be correct department
    if len(affiliations) == 1:
        return affiliations

    #Case 2. Try to return primary affiliation
    primary_affs = list(filter(lambda a: 'PRIMARY' in a, person['istEmployeeAffiliation']))
    if bool(primary_affs):
        return primary_affs

    #Case 3. No primary affiliation in D1
    normal_affs = list(filter(lambda a: 'NORMAL' in a, person['istEmployeeAffiliation']))
    if bool(normal_affs):
        return normal_affs

    raise AssertionError("can't parse affiliation {} of {}".format(affiliations, person['cn']))


def is_person_relevant(person):
    affiliations = get_relevant_affiliations(person)
    aff_markers = set()
    for a in affiliations:
        aff_markers.update(a.split(';'))
    for (marker, relevance) in ROLE_RELEVANCE_MAP:
        if marker in aff_markers:
            return relevance
    return ROLE_RELEVANCE_DEFAULT


def get_relevant_people(people):
    buckets = {True: [], False: []}
    for person in people:
        buckets[is_person_relevant(person)].append(person)
    return buckets[True], buckets[False]


if __name__=='__main__':
    people = compute_current_people()
    relevant_people, skipped_people = get_relevant_people(people)
    relevant_cns = list(map(lambda person: person['cn'][0], relevant_people))
    skipped_cns = list(map(lambda person: person['cn'][0], skipped_people))
    print('Relevant:', relevant_cns)
    print('Skipped:', skipped_cns)
    print('Sample:', relevant_people[0])
